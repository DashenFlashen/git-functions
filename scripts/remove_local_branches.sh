#!/bin/bash

rows=$(git branch -vv | grep ": gone")
filling="----------------------------------------------"

if [ -n "$rows" ] 
then
    echo $filling
    echo "Branches that are no longer tracked by remote:" 
    for branch in $(echo "$rows" | awk '{if($1 != "*") {print $1;} else print $2;}')
    do
        echo " -$branch"
    done
    echo $filling
    echo
fi

for branch in $(echo "$rows" | awk '{if($1 != "*") {print $1;} else print $2;}')
do
    read -p "Would you like to delete local branch '$branch' (y/[n])`echo $'\n> '`" choice
    case "$choice" in 
        y|Y ) git branch -D $branch;;
        n|N ) echo "No";;
        * ) echo "No";;
    esac
done

