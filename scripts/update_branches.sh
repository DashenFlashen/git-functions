#!/bin/bash

update_branches(){

    current_branch=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ \1/')
    for branch in $(echo "$rows" | awk '{if($1 != "*") {print $1;} else print $2;}')
    do
        git checkout $branch
        git merge origin/$branch
    done
    git checkout $current_branch
}

clean_repo=$(git status | grep "working tree clean")
rows=$(git branch -vv | grep ": behind")
filling="----------------------------------------------"

if [ -z "$clean_repo" ]; then
    echo "Repository is not clean. Remove or stash your local changes."
    echo "Exiting..."
    exit
fi

if [ -n "$rows" ] 
then
    echo $filling
    echo "Branches that are behind:" 
    for branch in $(echo "$rows" | awk '{if($1 != "*") {print $1;} else print $2;}')
    do
        echo " -$branch"
    done
    echo $filling
    echo

read -p "Would you like to update all branches? (y/[n])`echo $'\n> '`" choice
case "$choice" in 
    y|Y ) update_branches;;
    n|N ) echo "No";;
    * ) echo "No";;
esac
fi
